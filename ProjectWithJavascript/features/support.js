'use strict';
const axios = require('axios');
const assert = require('assert');

let url = 'https://petstore.swagger.io/v2/pet/';
let urlUser = 'https://petstore.swagger.io/v2/user/';

let returned;
let newNameData;
let newUserData;
let pom;
module.exports = {
    isItFriday: function (today) {
        return "No"
    },

    addPetToTheStore: async function (params) {
        let resp = await axios.post(url, params).then(response => {
            if (response.data) {

            }
        }).catch(error => {
            console.log(error.response.data);
            // console.log(error.response.status);
            // console.log(error.response.headers);
        })
    },
    findPetByIdAfterDelete: async function (int) {
        let resp = await axios.get(url + int).then(response => {

        }).catch(error => {
            console.log(error.response.data.message);
        })
    },
    getPetById: async function (int) {
        let resp = await axios.get(url + int).then(response => {
            if (response.data) {
                returned = response.data
                
            }
        }).catch(error => {
            console.log(error.response.data);
        })
        return returned
    },
    deletePetById: async function (int) {
        let resp = await axios.delete(url + int).then(response => {
            if (response.data) {
            }
        }).catch(error => {
            console.log(error.response.data);
        })
    },

    returnedPetWithData: async function (params, int) {
        this.getPetById(int).then(function (response) {
            pom = response
            //id u pom je int a u params je 'id'
            //assert.equal(pom,params)
        })
       
    },
    updatePetWithNewName: async function (dataTable) {
        
        newNameData = dataTable.hashes()


        let newName = newNameData[0].Name
        
        let resp = await axios.post(url + newNameData[0].Id + '?name=' + newName).then(response => {
            if (response.data) {
                // console.log(response.data)
            }
        }).catch(error => {
            console.log(error.response.data);
        })

      
        return newName
    },

    //------------------------------------------------------------------------------------------------------------

    addUserToTheStore: async function (params) {
        let resp = await axios.post(urlUser, params).then(response => {
            if (response.data) {
                //console.log(response.data.code)
            }
        }).catch(error => {
            console.log(error.response.data.message)
        })
    },
    getUserByUsername: async function (string) {
        let resp = await axios.get(urlUser + string).then(response => {
            if (response.data) {
                pom = response.data
            }
        }).catch(error => {
            console.log(error.response.data.message)
        })
        return pom
    },
    userData: async function (params, string) {
        this.getUserByUsername(string).then(function (response) {
            pom = response
            //id u pom je int a u params je 'id'
            // assert.equal(pom,params)
        })
    },
    updateUserData: async function (dataTable) {
        newUserData = dataTable.hashes()

        let username = newUserData[0].Username

        let resp = await axios.put(urlUser + username + newUserData).then(response => {
            if (response.data) {
                console.log(response.data)
            }
        }).catch(error => {
            console.log(error.response.data.message)
        })
    },
    deleteUserByUsername: async function (string) {
        let resp = await axios.delete(urlUser + string).then(response => {
            if (response.data) {
                console.log(response.data.code)
            }
        }).catch(error => {
            console.log(error.response.data.code)
        })
    },
    loginUser: async function (username, password) {
        let resp = await axios.get('https://petstore.swagger.io/v2/user/login?username=' + username + '&password=' + password).then(response => {
            if (response.data) {
                console.log(response.data.message)
            }
        }).catch(error => {
            console.log(error)
        })
    },
    logoutUser: async function () {
        let resp = await axios.get(urlUser + "logout").then(response => {
            if (response.data) {
                assert.equal(response.data.message, "ok")
            }
        }).catch(error => {
            console.log(error.response.data.message)
        })
    }
}