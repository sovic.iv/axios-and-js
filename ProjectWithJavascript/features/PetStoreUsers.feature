Feature: PetStore Users

    Scenario: Add new User
        Given I have User with following details
            | Id   | Username   | FirstName   | LastName   | Email   | Password   | Phone   | UserStatus   |
            | <Id> | <Username> | <FirstName> | <LastName> | <Email> | <Password> | <Phone> | <UserStatus> |
        Then I add this User to the Store
        When I get User by Username: "<Username>"
        Then User should be returned with correct data
        Then I send request to update User with new email
            | Id   | Username   | FirstName   | LastName   | NewEmail   | Password   | Phone   | UserStatus   |
            | <Id> | <Username> | <FirstName> | <LastName> | <NewEmail> | <Password> | <Phone> | <UserStatus> |
        When I login User with correct Username "<Username>" and Password "<Password>"
        Then I can logout current logged User
        When I send request to delete User with Username: "<Username>"
        Then I should be able to see that User is deleted
        Examples:
            | Id    | Username  | FirstName | LastName | Email              | Password    | Phone     | UserStatus | NewEmail           |
            | 5557  | SuperName | Sofija    | Jankovic | sofi@gmail.com     | password123 | 064598723 | 0          | sofija@gmail.com   |
            | 58796 | Petar88   | Petar     | Petrovic | petarp88@gmail.com | password345 | 065896325 | 0          | petar88p@gmail.com |
