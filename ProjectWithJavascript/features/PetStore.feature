Feature: PetStore
	1. Create and return a new Pet with:
	a. Id
	b. Category_name
	c. Pet_name
	d. Status
	e. tagName
	f. photoUrl
	2. Verify the Pet was created with correct data.
	3. Update this Pet_name, Verify update and return record.
	4. Delete the Pet and demonstrate pet now deleted.
	https://petstore.swagger.io/v2/pet

	#@smoke
	Scenario: Add Pet to the Store
		Given I have Pet with following details
			| Id   | CategoryId   | CategoryName   | Name   | Status   | TagId   | TagName   | PhotoURL   |
			| <Id> | <CategoryId> | <CategoryName> | <Name> | <Status> | <TagId> | <TagName> | <PhotoURL> |
		Then I Add this Pet to the Store
		When I get Pet by Id: <Id>
		Then Pet should be returned with correct data
		Then I send request to update Pet with new name
			| Id   | CategoryId   | CategoryName   | Name      | Status   | TagId   | TagName   | PhotoURL   |
			| <Id> | <CategoryId> | <CategoryName> | <NewName> | <Status> | <TagId> | <TagName> | <PhotoURL> |
		When I get Pet by Id: <Id>
		Then Pet should be returned with correct data
		When I send request to delete Pet with Id: <Id>
		Then I should be able to see that pet is deleted

		Examples:
			| Id      | CategoryId | CategoryName | Name    | Status    | TagId | TagName    | PhotoURL                                                                    | NewName  |
			| 1474093 | 71         | Dog          | Rea     | available | 30    | HouseDog   | https://upload.wikimedia.org/wikipedia/en/1/12/Brian_Griffin.png            | Jonathan |
			| 9999999 | 99         | Dog          | Zivorad | available | 99    | OutdoorDog | https://www.muskimagazin.rs/chest/timg/1496921016_SILVA%205_zps9cymrwcm.jpg | Simens   |