'use strict';
const { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require('constants');
const { Given, When, Then } = require('cucumber');
const support = require("../../features/support");
let pom;
let params;


Given('I have Pet with following details', async function (dataTable) {
    // Write code here that turns the phrase above into concrete actions
    this.data = dataTable.hashes()
    params =
    {
        "id": this.data[0].Id,
        "category": {
            "id": this.data[0].CategoryId,
            "name": this.data[0].CategoryName
        },
        "name": this.data[0].Name,
        "photoUrls": [
            this.data[0].PhotoURL,
        ],
        "tags": [
            {
                "id": this.data[0].TagId,
                "name": this.data[0].TagName
            }
        ],
        "status": this.data[0].Status
    }
});



Then('I Add this Pet to the Store', function () {
    support.addPetToTheStore(params)

});



When('I get Pet by Id: {int}', async function (int) {
    // When('I get Pet by Id: {float}', function (float) {
    // Write code here that turns the phrase above into concrete actions
    support.getPetById(int).then(function (response) {
        pom = response
    });



});


Then('Pet should be returned with correct data', async function () {
    support.returnedPetWithData(params, params.id)

});



Then('I send request to update Pet with new name', async function (dataTable) {
    support.updatePetWithNewName(dataTable).then(function (response) {
        params.name = response
    });



});

When('I send request to delete Pet with Id: {int}', async function (int) {
    // When('I send request to delete Pet with Id: {float}', function (float) {
    // // Write code here that turns the phrase above into concrete actions
    //setTimeout(() => { support.deletePetById(int) }, 5000);
    support.deletePetById(int)

});


Then('I should be able to see that pet is deleted', async function () {
    // Write code here that turns the phrase above into concrete actions
    support.findPetByIdAfterDelete(params.id)
    //setTimeout(() => { support.findPetByIdAfterDelete(ID) }, 5000);

});
