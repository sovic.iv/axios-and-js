'use strict';
const { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require('constants');
const { Given, When, Then } = require('cucumber');
const axios = require('axios');
const assert = require('assert');
const support = require("../../features/support");
let params;
let pom;

Given('I have User with following details', async function (dataTable) {
    // Write code here that turns the phrase above into concrete actions
    this.data = dataTable.hashes()
    params =
    {
        "id": this.data[0].Id,
        "username": this.data[0].Username,
        "firstName": this.data[0].FirstName,
        "lastName": this.data[0].LastName,
        "email": this.data[0].Email,
        "password": this.data[0].Password,
        "phone": this.data[0].Phone,
        "userStatus": this.data[0].UserStatus
    }

});

Then('I add this User to the Store', async function () {
    support.addUserToTheStore(params)
});

When('I get User by Username: {string}', async function (string) {
    support.getUserByUsername(string).then(function (response) {
        pom = response
        //  console.log(pom)
    })
    //console.log(pom)
});

Then('User should be returned with correct data', function () {
    // Write code here that turns the phrase above into concrete actions
    // support.userData(params, params.username)
    // console.log(params.username)
});


Then('I send request to update User with new email', function (dataTable) {
    // Write code here that turns the phrase above into concrete actions
    support.updateUserData(dataTable)
});

When('I login User with correct Username {string} and Password {string}', function (string, string2) {
    // Write code here that turns the phrase above into concrete actions
    support.loginUser(string, string2)
});

When('I send request to delete User with Username: {string}', function (string) {
    // Write code here that turns the phrase above into concrete actions
    support.deleteUserByUsername(string)
});


Then('I can logout current logged User', function () {
    // Write code here that turns the phrase above into concrete actions
    support.logoutUser()
});


Then('I should be able to see that User is deleted', function () {
    // Write code here that turns the phrase above into concrete actions
    support.getUserByUsername(params.username).then(function (response) {
        pom = response
        //console.log(pom)
    })
});